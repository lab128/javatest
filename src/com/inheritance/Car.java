package com.inheritance;

import java.util.Date;

public class Car extends Vehicle{
	public static void main(String[] args) {
		System.out.println("Test");
		Car car1 = new Car();
		car1.run();
	}
	//Here overrides Vehicle.run, also changes modifer to public, pero
	//los modifiers son factores que no califican para el overloading, tampoco el return type
	public void run() {
		//call parent run
		super.run();
		System.out.println("Run from Car");
	}
	
//	Tener diferentes return types para metodos con mismo nombres no califican para overloading
	//Por lo tanto los sig metodos ser�an metodos dupllicados, no sobrecargados
//	String a() {return null;}
//	Date   a() {return null;}
}
