package day1.exercises;

public class Friend extends Contact{
	public String phoneNumber;
	
	//static initialization block
	//which is executed when class is loaded by class loader.
	//is often used to create static variables.
	static {
        System.out.println("Step: class loader");
    }
	//non-static initializer block 
	//is created on object construction only, will have access to 
	//instance variables and methods, and will be called 
	//at the beginning of the constructor,
	{
        System.out.println("Step: initialization block3");
    }
	//Constructor
	public Friend() {
        System.out.println("Step: construct");
	}
	
	{
        System.out.println("Step: initialization block");
    }
}
